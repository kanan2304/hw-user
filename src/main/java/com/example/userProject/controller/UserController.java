package com.example.userProject.controller;

import com.example.userProject.dto.CreateUserDto;
import com.example.userProject.dto.UpdateUserDto;
import com.example.userProject.dto.UserDto;
import com.example.userProject.services.UserServices;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserServices userServices;

    public UserController(UserServices userServices){
        this.userServices=userServices;
    }
    @GetMapping("/login")
    public String login(@RequestBody UserDto user){
        return "Login with username: " + userServices.login(user) + "was successful";
    }
    @PostMapping("/register")
    public String register(@RequestBody CreateUserDto user){
        return "Registration with username: " + userServices.register(user) + "was successful";
    }
    @PutMapping("resetPassword")
    public String resetPassword(@RequestBody UpdateUserDto user){
        return "New Password is: " + userServices.resetPassword(user);
    }
}