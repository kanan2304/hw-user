package com.example.userProject.services;

import com.example.userProject.User;
import com.example.userProject.dto.CreateUserDto;
import com.example.userProject.dto.UpdateUserDto;
import com.example.userProject.dto.UserDto;
import com.example.userProject.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class UserServices {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    public UserServices(UserRepository userRepository, ModelMapper modelMapper){
        this.userRepository=userRepository;
        this.modelMapper=modelMapper;
    }
    public String register(CreateUserDto dto) {
        if(dto.getPassword().equals(dto.getRepeatpassword())) {
            User user = modelMapper.map(dto, User.class);
            userRepository.save(user);
            return new UserDto().getUsername();
        } else{
            throw new RuntimeException();
        }
    }
    public String login(UserDto dto) {
        User user = new User();
        if(dto.getUsername().equals(userRepository) & dto.getPassword().equals(userRepository)) {
            UserDto userDto = modelMapper.map(user, UserDto.class);
            return new UserDto().getUsername();
        }else{
            throw new RuntimeException();
        }
    }
    public UserDto resetPassword(UpdateUserDto dto) {
        User user = modelMapper.map(dto, User.class);
        return new UserDto();
    }
}