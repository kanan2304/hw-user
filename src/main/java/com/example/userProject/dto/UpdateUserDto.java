package com.example.userProject.dto;

import lombok.Data;

@Data
public class UpdateUserDto {
    Integer id;
    String username;
    String password;
    String repeatpassword;
    String email;
}
