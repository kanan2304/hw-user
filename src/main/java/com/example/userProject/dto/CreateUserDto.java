package com.example.userProject.dto;

import lombok.Data;

@Data
public class CreateUserDto {
    String username;
    String password;
    String repeatpassword;
    String email;

}
