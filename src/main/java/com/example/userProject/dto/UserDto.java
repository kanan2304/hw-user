package com.example.userProject.dto;

import lombok.Data;

@Data
public class UserDto {
    String username;
    String password;
}